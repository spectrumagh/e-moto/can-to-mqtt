CC = g++
CFLAGS = -std=c++20 -g
LIBS = -lpaho-mqttpp3 -lpaho-mqtt3as -lyaml-cpp
TARGET = can2mqtt
SRC = src/main.cpp src/parsers/ConfigParser.cpp src/parsers/CanParserDataFilter.hpp src/parsers/CanParser.hpp src/parsers/CanParserRequest.hpp

BUILD_DIR = build

all: $(TARGET)

$(TARGET): $(SRC)
	mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) -o $(BUILD_DIR)/$(TARGET) $(SRC) $(LIBS)

clean:
	rm -rf $(BUILD_DIR)

install: all
	sudo systemctl stop can2mqtt.service | true
	sudo mkdir -m 0655 -p /opt/can2mqtt/ /opt/can2mqtt/conf.d/
	sudo cp $(BUILD_DIR)/$(TARGET) /opt/can2mqtt/
	sudo cp ./conf.d/*.yaml /opt/can2mqtt/conf.d/
	sudo cp can2mqtt.service /etc/systemd/system/
	sudo systemctl daemon-reload
	sudo systemctl enable --now can2mqtt.service

# logs
# journalctl -u can2mqtt -f 

remove:
	sudo systemctl disable --now can2mqtt.service
	sudo rm /etc/systemd/system/can2mqtt.service 
	sudo systemctl daemon-reload
	sudo rm -rf /opt/can2mqtt/
