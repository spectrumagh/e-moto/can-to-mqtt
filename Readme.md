# Emoto Can 2 Mqtt

### App schema

```mermaid
flowchart LR
    socket_can["SocketCan
    ex. can0"]
    app["Can2Mqtt (this repo)
    Processing incoming can frames
    Sending can requests"]
    mqtt["MQTT
    ex. Mosquitto"]

    click socket_can "https://docs.kernel.org/networking/can.html" "SocketCan"

    click mqtt "https://mosquitto.org/" "Mosquitto"

    socket_can <--CAN_RAW--> app

    app --MQTT--> mqtt

```

### Deps

```sh
sudo apt install libpaho-mqttpp-dev libpaho-mqtt-dev libyaml-cpp-dev
sudo apt install build-essential gdb
```

### Useful link

VirtualCan: https://netmodule-linux.readthedocs.io/en/1.2.4/howto/can.html