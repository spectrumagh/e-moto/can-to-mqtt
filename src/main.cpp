#include <mqtt/client.h>
#include <ostream>
#include <filesystem>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <cstdint>
#include <unistd.h>

#include <thread>
#include <chrono>

#include "parsers/ConfigParser.h"

// https://makesomeshit.com/how-to-mqtt-in-cpp-c/

#define DOUBLE_TOPIC_PREFIX "double/"

void mqttPublish(mqtt::client &client, std::string &topicName, std::string payload)
{
    mqtt::message_ptr timeLeftMessagePointer = mqtt::make_message(topicName, payload);
    // Publish the Mqtt message using the connected client.
    client.publish(timeLeftMessagePointer);
}

void sendFrameWithoutDef(can_frame &frame, mqtt::client &mqttClient)
{
    std::cout << "No transformations found for 0x" << std::hex << frame.can_id << std::endl;
    // char canIdChars [4];
    // char canPayloadChars[frame.can_dlc*2];

    // sprintf(canIdChars, "%02X", frame.can_id);
    // for (int i = 0; i < frame.can_dlc; i++)
    // {
    //     sprintf(canPayloadChars + i*2 , "%02X", frame.data[i]);
    //     // todo x2
    // }

    // mqttPublish(mqttClient, "aa", "lol");
}

// calculate value for array
uint64_t calculateFromArray(unsigned char data[], uint8_t &length, uint8_t &offset, bool &reversed)
{
    uint64_t result = 0;

    if (!reversed)
    {
        // Ox1122 -> reads: 0x11 << 8 + 0x22
        for (int i = 0; i < length; i++)
        {
            result = (data[i + offset] << (8 * (length - 1 - i))) | result;
        }
    }
    else
    {
        // Ox1122 -> reads reversed: 0x22 << 8 + 0x11
        // sevcon case
        for (int i = 0; i < length; i++)
        {
            result = (data[i + offset] << (8 * i)) | result;
        }
    }
    return result;
}

void sendFrameWithDef(can_frame &frame, mqtt::client &mqttClient, CanParser &canTrans)
{
    std::string topicName = DOUBLE_TOPIC_PREFIX + canTrans.topic;
    std::cout << "Transformations found for 0x" << std::hex << frame.can_id << " for topic " << topicName << std::endl;

    uint64_t concactedHEX = calculateFromArray(frame.data, canTrans.length, canTrans.offset, canTrans.reversed);
    double result = concactedHEX * canTrans.scale;

    mqttPublish(mqttClient, topicName, std::to_string(result));
}

// find can transformations for specific frame id
CanTransformationsForFrame_t* findCanTrans(CanTransformationsForFrames_t &config, can_frame &frame)
{
    if (config.find(frame.can_id) == config.end())
    {
        return nullptr;
    }
    return &config.at(frame.can_id);
}

bool checkIfDataInFrameMatchTransformationFilter(CanParser &transformation, uint8_t *frameData)
{
    for (uint8_t i = 0; i < transformation.data.filterLen; i++)
    {
        if (frameData[i + transformation.data.offset] != transformation.data.filter[i])
        {
            return false;
        }
    }
    return true;
}

void processCANFrame(CanTransformationsForFrames_t &configMap, can_frame &frame, int nbytes, mqtt::client &mqttClient)
{
    // discard can id extended frame
    frame.can_id = frame.can_id & (~CAN_EFF_FLAG);

    int i = 0;
    if (nbytes < 0)
    {
        perror("Read");
        return;
    }

    // find configurations - informations for one can frame
    auto transformationsForOneFrame = findCanTrans(configMap, frame);
    if (transformationsForOneFrame)
    {
        // for every transformations in one frame
        for(auto transformation : *transformationsForOneFrame)
        {
            if(checkIfDataInFrameMatchTransformationFilter(transformation, frame.data))
            {
                sendFrameWithDef(frame, mqttClient, transformation);
            }
        }
        return;
    }
    // transformations not found
    sendFrameWithoutDef(frame, mqttClient);
}

void CanOpenRequestSender_thread(uint16_t ms, CanTransformationsForFrame_t transformationsForOneFrame, int cansock)
{
    std::cout << "Ms: " << std::to_string(ms) << " [ms]" << std::endl;

    while (true)
    {
        for (auto configForFrame : transformationsForOneFrame)
        {
            struct can_frame txmsg;
            txmsg.can_id = configForFrame.req.frameId;
            if(configForFrame.req.frameId > 0xfff)
            {
                // extended frame
                txmsg.can_id |= CAN_EFF_FLAG;
            }

            txmsg.can_dlc = configForFrame.req.msgLen;
            for (uint8_t i = 0; i < configForFrame.req.msgLen; i++)
            {
                txmsg.data[i] = configForFrame.req.msg[i];
            }

            // todo extended frame support
            write(cansock, &txmsg, sizeof(txmsg));
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(ms));
    }
    std::cout << "Exiting " << ms << " [ms] thread" << std::endl;
}

// With the library header files included, continue by defining a main function.
int main(int argc, char **argv)
{
    std::string devName = (argc > 1 ? argv[1] : "vcan0");
    std::cout << "Can device: " << devName << std::endl;

    // In order to connect the mqtt client to a broker,
    // Define an Ip address pointing to a broker. In this case, the localhost on port 1883.
    std::string ip = "localhost:1883";
    // Then, define an ID to be used by the client when communicating with the broker.
    std::stringstream id;
    id << "can2mqtt_pid_" << std::to_string(getpid());

    // Construct a client using the Ip and Id, specifying usage of MQTT V5.
    mqtt::client client(ip, id.str(), mqtt::create_options(MQTTVERSION_5));
    // Use the connect method of the client to establish a connection to the broker.
    client.connect();
    // Initialize an empty message with specified topic.

    std::cout << "Client " << id.str() << " connected to " << ip << std::endl;

    // ----------------    can   ------------------------

    int s;
    int nbytes;
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;

    printf("CAN Sockets Receive Demo\r\n");

    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        perror("Socket");
        return 1;
    }

    strcpy(ifr.ifr_name, devName.c_str());
    ioctl(s, SIOCGIFINDEX, &ifr);

    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Bind");
        return 1;
    }

    std::vector<std::string> configFilesPath = std::vector<std::string>();

    for (const auto & entry : std::filesystem::directory_iterator("conf.d"))
    {
        auto path = entry.path();
        if(path.extension() == ".yaml" || path.extension() == ".yml")
        {
            configFilesPath.push_back(path);
        }
    }

    ConfigParser configParser = ConfigParser(configFilesPath);
    CanTransformationsForFrames_t config = configParser.GetConfigsForParsingIncomingCanFrames();
    
    auto requestsConfig = configParser.GetConfigsToRequestForCanFrames();
    for (const auto i : requestsConfig)
    {
        std::thread thr(CanOpenRequestSender_thread, i.first, i.second, s);
        thr.detach();
    }

    while (1)
    {
        nbytes = read(s, &frame, sizeof(struct can_frame));
        processCANFrame(config, frame, nbytes, client);
    }

    if (close(s) < 0)
    {
        perror("Close");
        return 1;
    }

    return 0;
}

// sudo apt install libpaho-mqttpp-dev libpaho-mqtt-dev libyaml-cpp-dev
// g++ main.cpp  -lpaho-mqttpp3 -lpaho-mqtt3as -o main