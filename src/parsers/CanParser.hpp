#ifndef CanParser_H
#define CanParser_H

#include <linux/can.h>
#include <cstdint>
#include <yaml-cpp/yaml.h>
#include "CanParserDataFilter.hpp"
#include "CanParserRequest.hpp"

#define OPEN_CAN_STARTING_FRAME_ID 0x580


struct CanParser
{
    canid_t frameId = 0;
    std::string topic = std::string(); // empty string
    
    uint8_t offset = 0;
    uint8_t length = 1;
    double scale = 1.0;

    bool reversed = false;

    // regex na wartości w ramkach
    CanParserDataFilter data;
    CanParserRequest req;
};

namespace YAML
{
  template<> struct convert<CanParser>
  {
    // "encode() is not implemented"; 
    static Node encode(const CanParser& rhs)
    {
      return Node();
    }

    static bool decode(const Node& node, CanParser& rhs)
    {
      if(!node.IsMap())
      {
        return false;
      }

      if(node["topic"])
      {
        rhs.topic = node["topic"].as<std::string>();
      }

      if(node["frameId"])
      {
        rhs.frameId = node["frameId"].as<canid_t>();
      }
    
      if(node["offset"])
      {
        rhs.offset = node["offset"].as<uint8_t>();
      }

      if(node["length"])
      {
        rhs.length = node["length"].as<uint8_t>();
      }

      if(node["scale"])
      {
        rhs.scale = node["scale"].as<double>();
      }

      if(node["reversed"])
      {
        rhs.reversed = node["reversed"].as<bool>();
      }

      if(node["data"])
      {
        rhs.data = node["data"].as<CanParserDataFilter>();
      }

      if(node["req"])
      {
        rhs.req = node["req"].as<CanParserRequest>();
      }

      return true;
    }
  };
}

#endif
