#ifndef CanParserDataFilter_H
#define CanParserDataFilter_H

#include <linux/can.h>
#include <yaml-cpp/yaml.h>

struct CanParserDataFilter
{
    uint8_t* filter = nullptr;
    uint8_t filterLen = 0;
    uint8_t offset = 0;
};

namespace YAML
{
  template<> struct convert<CanParserDataFilter>
  {
    // "encode() is not implemented"; 
    static Node encode(const CanParserDataFilter& rhs)
    {
      return Node();
    }

    static bool decode(const Node& node, CanParserDataFilter& rhs)
    {
      if(!node.IsMap())
      {
        return false;
      }

      if(node["offset"])
      {
        rhs.offset = node["offset"].as<uint8_t>();
      }

      auto matchDataNode = node["filter"];
      if (matchDataNode)
      {
        rhs.filterLen = matchDataNode.size();
        rhs.filter = new uint8_t[rhs.filterLen];
        for (std::size_t i = 0; i < matchDataNode.size(); i++)
        {
          rhs.filter[i] = matchDataNode[i].as<uint8_t>();
        }
      }

      return true;
    }
  };
}

#endif
