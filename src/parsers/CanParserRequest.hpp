#ifndef CanParserRequests_H
#define CanParserRequests_H

#include <cstdio>
#include <linux/can.h>
#include <yaml-cpp/yaml.h>

struct CanParserRequest
{
    uint16_t ms = 0;
    uint8_t* msg = nullptr;
    uint8_t msgLen = 0;
    canid_t frameId = 0;
};

namespace YAML
{
  template<> struct convert<CanParserRequest>
  {
    // "encode() is not implemented"; 
    static Node encode(const CanParserRequest& rhs)
    {
      return Node();
    }

    static bool decode(const Node& node, CanParserRequest& rhs)
    {
      if(!node.IsMap())
      {
        return false;
      }

      auto reqMsgNode = node["msg"];
      if (reqMsgNode)
      {
        rhs.msgLen = reqMsgNode.size();
        rhs.msg = new uint8_t[rhs.msgLen];
        for (std::size_t i = 0; i < rhs.msgLen; i++)
        {
          rhs.msg[i] = reqMsgNode[i].as<uint8_t>();
        }
      }

      if(node["frameId"])
      {
        rhs.frameId = node["frameId"].as<canid_t>();
      }

      if(node["ms"])
      {
        rhs.ms = node["ms"].as<uint16_t>();
      }

      return true;
    }
  };
}

#endif
