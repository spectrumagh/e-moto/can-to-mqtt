#include <string>
#include <yaml-cpp/yaml.h>
#include "CanParser.hpp"
#include "ConfigParser.h"
#include <linux/can.h>
#include <iostream>

using namespace std;

void ConfigParser::AddConfigToProperDataSets(CanParser &canTransformation)
{
    if (!canTransformation.topic.empty() && canTransformation.frameId != 0)
    {
        canConfig[canTransformation.frameId].push_back(canTransformation);
        std::cout << "+ Added incoming parser for 0x" << std::hex << canTransformation.frameId << " on topic " << canTransformation.topic << std::endl;
    }

    if (canTransformation.req.ms != 0 && canTransformation.req.msg != nullptr)
    {
        canConfigWithRequests[canTransformation.req.ms].push_back(canTransformation);
    }
}

void ConfigParser::ParseGenericCanFrame(YAML::Node &yamlConfig)
{
    for (auto yamlCanConfigItem : yamlConfig["Can"])
    {
        PrintNode(yamlCanConfigItem);
        auto canTransformation = yamlCanConfigItem.as<CanParser>();
        AddConfigToProperDataSets(canTransformation);
    }
}

void ConfigParser::ParseCanOpenSDOFrame(YAML::Node &yamlCanOpenSDO)
{
    canid_t frameId = yamlCanOpenSDO["nodeId"].as<uint8_t>() + OPEN_CAN_STARTING_FRAME_ID;
    bool reversed = yamlCanOpenSDO["reversed"].as<bool>();
    
    uint8_t senderMeta = 0;
    if (yamlCanOpenSDO["senderMeta"])
    {
        senderMeta = yamlCanOpenSDO["senderMeta"].as<uint8_t>();
    }
    
    auto yamlCanOpenSDOItems = yamlCanOpenSDO["items"];
    // ważne const referencja bo yaml node są złe
    for (const auto &yamlCanOpenSDOItem : yamlCanOpenSDOItems)
    {
        auto yamlCanOpenSDOItemIndex = yamlCanOpenSDOItem["index"].as<uint16_t>();
        for (const auto &canOpenSDOitemInIndex : yamlCanOpenSDOItem["items"])
        {
            PrintNode(canOpenSDOitemInIndex);
            auto subindex = canOpenSDOitemInIndex["subindex"].as<uint8_t>();
            auto canTransformation = canOpenSDOitemInIndex.as<CanParser>();

            canTransformation.frameId = frameId;
            canTransformation.reversed = reversed;

            // add 4 bc, first 4 bytes are can open meta data
            canTransformation.offset += 4;

            // can open frame
            // | metadata | index_1 | index_2 | subindex | data x4 |

            canTransformation.data.filter = new uint8_t[3];
            canTransformation.data.filterLen = 3;


            if (!reversed)
            {
                // 0x(index_1+index_2)
                canTransformation.data.filter[0] = yamlCanOpenSDOItemIndex >> 8;
                canTransformation.data.filter[1] = yamlCanOpenSDOItemIndex;
            }
            else
            {
                // 0x(index_2+index_1)
                canTransformation.data.filter[0] = yamlCanOpenSDOItemIndex;
                canTransformation.data.filter[1] = yamlCanOpenSDOItemIndex >> 8;
            }
            canTransformation.data.filter[2] = subindex;

            canTransformation.data.offset = 1;


            // construct request payload based on response data
            canTransformation.req.msg = new uint8_t[8]{0};

            canTransformation.req.msg[0] = senderMeta;
            canTransformation.req.msg[1] = canTransformation.data.filter[0];
            canTransformation.req.msg[2] = canTransformation.data.filter[1];
            canTransformation.req.msg[3] = canTransformation.data.filter[2];

            canTransformation.req.frameId = frameId + 0x80;
            canTransformation.req.msgLen = 8;

            AddConfigToProperDataSets(canTransformation);
        }
    }
}

ConfigParser::ConfigParser(std::vector<std::string> &configFilesPath)
{
    for (const auto &configFileName : configFilesPath)
    {
        std::cout << "Parsing " << configFileName << std::endl;
        YAML::Node yamlConfig = YAML::LoadFile(configFileName);
        if (yamlConfig["Can"])
        {
            ParseGenericCanFrame(yamlConfig);
        }
        if (yamlConfig["CanOpenSDO"])
        {
            auto yamlCanOpenSDO = yamlConfig["CanOpenSDO"];
            ParseCanOpenSDOFrame(yamlCanOpenSDO);
        }
    }
}

const CanTransformationsForFrames_t &ConfigParser::GetConfigsForParsingIncomingCanFrames()
{
    return canConfig;
}

const CanTransformationsForFrames_t &ConfigParser::GetConfigsToRequestForCanFrames()
{
    return canConfigWithRequests;
}

void ConfigParser::PrintNode(YAML::Node &yamlNode)
{
    std::cout << std::endl
              << "--------------------" << std::endl;
    std::cout << YAML::Dump(yamlNode);
    std::cout << std::endl
              << "--------------------" << std::endl;
}

void ConfigParser::PrintNode(const YAML::detail::iterator_value &yamlNode)
{
    std::cout << std::endl
              << "--------------------" << std::endl;
    std::cout << YAML::Dump(yamlNode);
    std::cout << std::endl
              << "--------------------" << std::endl;
}
