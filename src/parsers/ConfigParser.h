#ifndef ConfigParser_H
#define ConfigParser_H

#include <cstdio>
#include <map>
#include <vector>
#include <linux/can.h>
#include "CanParser.hpp"

using namespace std;

typedef std::vector<CanParser> CanTransformationsForFrame_t;
typedef std::map<canid_t, CanTransformationsForFrame_t> CanTransformationsForFrames_t;

class ConfigParser
{
private:
    CanTransformationsForFrames_t canConfig;
    CanTransformationsForFrames_t canConfigWithRequests;

    void ParseGenericCanFrame(YAML::Node &yamlConfig);
    void ParseCanOpenSDOFrame(YAML::Node &yamlCanOpenSDO);

    void PrintNode(YAML::Node &yamlNode);
    void PrintNode(const YAML::detail::iterator_value &yamlNode);

    void AddConfigToProperDataSets(CanParser& canTransformation);
public:
    ConfigParser(std::vector<std::string>& configFilesPath);
    const CanTransformationsForFrames_t& GetConfigsForParsingIncomingCanFrames();
    const CanTransformationsForFrames_t& GetConfigsToRequestForCanFrames();
};

#endif
